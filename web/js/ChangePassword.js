$('#changepasswordbtn').click(function () {
    var dialog = $("#changePasswordPopoup").data('dialog');
    dialog.open();
});
$('#closePopup').click(function () {
    var dialog = $("#changePasswordPopoup").data('dialog');
    dialog.close();
});

$('#changePassword').click(function () {
    oldpass = $('#oldpass').val();
    newpass = $('#newpass').val();
    repeatpass = $('#repeatpass').val();
    id = $('#userid').val();
    if (oldpass != "" && newpass != "" && repeatpass != "") {
        if (newpass == repeatpass) {

            $.ajax({
                type: "post",
                url: "/userajax/changePassword",
                data: {id: id, oldpass: oldpass, newpass: newpass},
                success: function (result)
                {
                    if (result == "Done") {
//                        alert(result);
                    } else {
                        $('#passmsg').html("Old Password Is Wrong");
                    }
                    var dialog = $("#changePasswordPopoup").data('dialog');
                    dialog.close();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown)
                {

                    alert('Error : ' + errorThrown);

                }
            });

        } else {
            $('#passmsg').html("Not Matched");
        }

    } else {
        $('#passmsg').html("Can't Be Blank");
    }

});

