data = '';
edit_btn = '';
editcomment = function (id) {
    edit_btn = id.split('x')[0];
    data = $('#' + edit_btn + 'p').html();
    //alert(data);
    $('#' + edit_btn + 'p').html("<input type='text' value='" + data + "' class='editclass' id='edittext'/>");
    $('#comment').hide();
    $('#comment_btn').hide();
    $('#' + edit_btn + 'xd').hide();
    $('#' + edit_btn + 'xu').hide();



$('.editclass').keydown(function (e) {
    if (e.which == 13) {
        text = $('#edittext').val();
        if (text != '') {
            $.ajax({
                type: "POST",
                url: "/userajax/editcomment",
                data: {id: edit_btn, body: text},
                success: function (result)
                {
                    //alert(result);
                    $('#' + edit_btn + 'p').html(text);
                    $('#comment').show();
                    $('#comment_btn').show();
                    $('#' + edit_btn + 'xd').show();
                    $('#' + edit_btn + 'xu').show();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown)
                {
                    alert('Error : ' + errorThrown);
                }
            });
        } else {
            $('#' + edit_btn + 'p').html(data);
            $('#comment').show();
            $('#comment_btn').show();
            $('#' + edit_btn + 'xd').show();
            $('#' + edit_btn + 'xu').show();
        }
    }
});

};
