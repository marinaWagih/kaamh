var socket;
//window.onload=init();
function init() {
    var host = "ws://127.0.0.1:9000"; // SET THIS TO YOUR SERVER
    try {
        socket = new WebSocket(host);
        console.log('WebSocket - status ' + socket.readyState);
        socket.onopen = function (msg) {
            console.log("Welcome - status " + this.readyState);
        };
        socket.onmessage = function (msg) {
            data = msg.data.split(':');
            url = window.location.href.split('/');
            console.log(url);
            if (url[4] == data[6]) {
                appended = '<div class="list" id="' + data[4] + '">';
                console.log(appended);
                if (ownerdata[0] == 1) {
                    appended += '<img src="' + data[3] + '" style="width:50px;height:50px;"' + '/>'
                            + '<span class="list-title">anonymous</span>';
                } else {
                    appended += '<img src="' + data[3] + '" style="width:50px; height: 50px;"/>'
                            + '<span class="list-title">' + data[1] + '</span>';
                }
                appended += '<div class = "comment-content">';
                appended += '<div class="padding20 op-white">';
                appended += '<p id ="' + data[4] + 'p">' + data[5] + '</p>';
                appended += '</div>';
                appended += '</div>';
                appended += '</div>';
                //
                $('#commentContainer').append(appended);

            }
//console.log(msg.data);
        };
        socket.onclose = function (msg) {
            console.log("Disconnected - status " + this.readyState);
        };
    }
    catch (ex) {
        console.log(ex);
    }

}
$(window).load(init());

function quit() {
    if (socket != null) {
        log("Goodbye!");
        socket.close();
        socket = null;
    }
}

function reconnect() {
    quit();
    init();
}
$('#comment').keydown(function (e) {
    if (e.which == 13) {
        comment = $('#comment').val();
        if (comment != '') {
            expid = $('#expid').val();
            ownerdata = $('#ownerdata').val().split(':');
            arr = $('#ownerdata').val();
            $.ajax({
                type: "POST",
                url: "/userajax/addcomment",
                data: {comment: comment, expid: expid},
                success: function (result)
                {
                    //alert(result);
                    $('#comment').val('');
                    //
                    arr += ":" + result + ":" + comment + ":" + expid;
                    appended = '<div class="list" id="' + result + '">';
                    console.log(appended);
                    if (ownerdata[0] == 1) {
                        appended += '<img src="' + ownerdata[3] + '" style="width:50px;height:50px;"' + '/>'
                                + '<span class="list-title">anonymous</span>';
                    } else {
                        appended += '<img src="' + ownerdata[3] + '" style="width:50px; height: 50px;"/>'
                                + '<span class="list-title">' + ownerdata[1] + '</span>';
                    }
                    appended += '<div class = "comment-content">';
                    appended += '<div class="padding20 op-white">';
                    appended += '<div class ="Btns place-right">';
                    appended += '<button id ="' + result + 'xu" onclick ="editcomment(this.id)">';
                    appended += '<span class ="mif-pencil"></span>';
                    appended += '</button>';
                    appended += "<button id ='" + result + "xd' onclick = 'deletecomment(this.id)'>"
                            + " <i class = 'fa fa-times' > </i>"
                            + "</button>";
                    appended += '</div>';
                    appended += '<p id ="' + result + 'p">' + comment + '</p>';
                    appended += '</div>';
                    appended += '</div>';
                    appended += '</div>';
                    //
                    $('#commentContainer').append(appended);
                    try {
                        socket.send(arr);
                    } catch (ex) {
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown)
                {
                    alert('Error : ' + errorThrown);
                }
            });
        }
    }
});
