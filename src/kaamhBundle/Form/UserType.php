<?php

namespace kaamhBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('fristName', 'text')
                ->add('lastName', 'text')
                ->add('email', 'email')
                ->add('password', 'password')
                ->add('gender', 'choice', array(
                    'choices' => array('male' => 'Male', 'female' => 'Female'),
                    'multiple' => false,
                    'expanded' => true,
                    'required' => true,
                    'data' => 'male',
                    'disabled' => false
                ))
                ->add('profilePicture', 'file', array('data_class' => null))
//                ->add('isHidden', 'choice', array(
//                    'choices' => array('0' => 'Shown', '1' => 'Hidden'),
//                    'required' => true,
//                    'label' => 'Account :'
//                ))
                ->add('isHidden', 'checkbox', array(
                    'required' => true,
                ))
                
                ->add('isActive', 'checkbox', array(
                    'required' => true,
                ))
//                ->add('isActive', 'choice', array(
//                    'choices' => array('1' => 'Active', '0' => 'deactive'),
//                    'required' => true,
//                    'placeholder' => 'Choose your Account Activation level',
//                    'label' => 'Account :'
//                ))
        //           ->add('faceBookToken')
        //            ->add('isAdmin')
        //            ->add('createDate')
        //            ->add('logoutDate')
        //             ->add('interests'
        //                    ,'entity', array(
        //                        
        //                'class' => 'kaamhBundle:Tag',
        //                'query_builder' => function(EntityRepository $er) {
        //            return $er->createQueryBuilder('c')
        //                ; // You can add anything else here or
        //                  // use an existing function of the repository file
        //                },
        //    'required'  => true,
        //)
        //                        )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'kaamhBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'kaamhbundle_user';
    }

}
