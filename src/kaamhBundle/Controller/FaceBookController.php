<?php

namespace kaamhBundle\Controller;

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\GraphObject;
use Facebook\GraphUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use kaamhBundle\Entity\User;
use kaamhBundle\Helper\FaceBookHR;
use kaamhBundle\Helper\UserHR;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class FaceBookController extends Controller {
   
    protected $fbObj;
    public function __construct() 
    {
        $this->fbObj = new FaceBookHR();
    }

    private function index($session) {

      
        if (gettype($session) == 'string') 
        {
            echo '<a href="' . $session . '">login</a>';
        } 
        else 
        {
            
            $user_profile = $session;
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('kaamhBundle:User');
            $existuser = $repository->findOneBy(array('email' => $user_profile->getEmail()));
            if (!$existuser) {
                $dir ="images/profilePicture";
                $imgName = rand(1, 99999) . '' . rand(1, 99999) . rand(1, 99999) . '' . rand(1, 99999);
                copy('http://graph.facebook.com/' . $user_profile->getId() . '/picture', $dir . "/" . $imgName . ".jpg");
                $user = new User();
                $user->setEmail($user_profile->getEmail());
                $user->setFristName($user_profile->getFirstName());
                $user->setLastName($user_profile->getLastName());
                $user->setGender($user_profile->getGender());
                $user->setProfilePicture($dir . "/" . $imgName . ".jpg");
                $user->setFaceBookId($user_profile->getId());
                $em->persist($user);
                $em->flush();
                $token = new UsernamePasswordToken($user, $user->getPassword(), "default", array('ROLE_USER'));
                $securityContext = $this->container->get('security.context'); // do it your way
                $securityContext->setToken($token);
                $event = new InteractiveLoginEvent($this->getRequest(), $token);
                $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
                $this->getRequest()->getSession()->set('_security_secured_area', serialize($token));
                $this->getRequest()->getSession()->set('justReg','true');
                
                return $this->redirect('/home');
            } else {
                $error = 'You are already registerd';
            }
        }

        return $this->redirect($this->generateUrl('login', array('error' => $error)));

    }

    public function loginAction() {

        $fbObj = new FaceBookHR();
        $session =  $fbObj->getFacebookUser('login');
        if (gettype($session) == 'string') {
            echo '<a href="' . $session . '">login</a>';
        } else {
            $user_profile = $session;
            $email = $user_profile->getEmail();
            $em = $this->getDoctrine()->getEntityManager();
            $repository = $em->getRepository('kaamhBundle:User');
            $user = $repository->findOneBy(array('email' => $email));
            if ($user) 
            {
//                $session = $this->getRequest();
                $token = new UsernamePasswordToken($user, $user->getPassword(), "default", array('ROLE_USER'));
                $securityContext = $this->container->get('security.context'); // do it your way
                $securityContext->setToken($token);
                $event = new InteractiveLoginEvent($this->getRequest(), $token);
                $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
                $this->getRequest()->getSession()->set('_security_secured_area', serialize($token));
                $this->getRequest()->getSession()->set('justReg','false');
                 return $this->redirect('/home');
            }
            else
            {
                $this->index($session);  
            }
        }
        return $this->redirect($this->generateUrl('login'));
    }

}
