<?php

namespace kaamhBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use kaamhBundle\Entity\Experience;
use kaamhBundle\Entity\Comment;
use kaamhBundle\Entity\Tag;
use kaamhBundle\Entity\Rate;
use kaamhBundle\Form\ExperienceType;
use Symfony\Component\HttpFoundation\Session\Session;
use kaamhBundle\Entity\Report;
use kaamhBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use kaamhBundle\Helper\UploadImgHR;
use kaamhBundle\Entity\TagExperience;
use kaamhBundle\Helper\UserHR;

/**
 * Experience controller.
 *
 */
class ExperienceController extends Controller {

    /**
     * Lists all Experience entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
//        $session = new Session();
//           $user=$session->get('login');
//               $owner= $user->getId();
        // $entities = $em->getRepository('kaamhBundle:Experience')->findBy(array('user' => $owner));
        $entities = $em->getRepository('kaamhBundle:Experience')->findAll();
        return $this->render('kaamhBundle:Experience:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    public function newAction() {
//        var_dump($this->container->get('security.context')->getToken()->getUser());
//        exit();
        $experience = new Experience();
        $form = $this->createForm(new ExperienceType(), $experience, array('method' => 'POST'));
        $request = $this->get('request');
        $form->handleRequest($request);
        $repository = $this->getDoctrine()->getRepository('kaamhBundle:Tag');
        $tags = $repository->findAll();
//        $session = $this->getRequest()->getSession();
        $regUser = $this->container->get('security.context')->getToken()->getUser();
        if(gettype($regUser)=="string"){
           return $this->redirect("/login");
        }
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('kaamhBundle:User')->find($regUser->getId());
        if ($request->getMethod() == "POST") {

            if ($form->isValid()) {
                $writer = $em->getRepository('kaamhBundle:User')->find($user);
                $experience->setUser($writer);
                $experience->setPicture((new UploadImgHR())->UploadImg($form, 'experience', 'picture'));
                $em->persist($experience);
                $em->flush();
                $choosenTags = explode(",", $form->get('choosenTags')->getData());

                for ($i = 0; $i < count($choosenTags); $i+=3) {
                    if ($choosenTags[$i] != null) {
                        $tag = $em->getRepository('kaamhBundle:Tag')->find($choosenTags[$i]);
                        $experiencetag = new TagExperience();
                        $experiencetag->setTag($tag);
                        $experiencetag->setExperience($experience);
                        $em->persist($experiencetag);
                        $em->flush();
                    } else {
                        $exist = $em->getRepository('kaamhBundle:Tag')->findBy(array("name" => strtolower($choosenTags[$i + 1])));
                        if (!$exist) {
                            $parent = $em->getRepository('kaamhBundle:Tag')->find($choosenTags[0]);
                            $newtag = new Tag();
                            $newtag->setName($choosenTags[$i + 1]);
                            $newtag->setParent($parent);
                            $newtag->setUser($writer);
                            $em->persist($newtag);
                            $em->flush();
                            $experiencetag = new TagExperience();
                            $experiencetag->setTag($newtag);
                            $experiencetag->setExperience($experience);
                            $em->persist($experiencetag);
                            $em->flush();
                        } else {
                            $experiencetag = new TagExperience();
                            $experiencetag->setTag($exist);
                            $experiencetag->setExperience($experience);
                            $em->persist($experiencetag);
                            $em->flush();
                        }
                    }
                }

                return $this->redirect($this->generateUrl('experience_show', array('id' => $experience->getId())));
            }
        }
        return $this->render('kaamhBundle:Experience:new.html.twig', array(
                    'entity' => $experience,
                    'form' => $form->createView(),
                    'tags' => $tags
        ));
    }

    /**
     * Finds and displays a Experience entity.
     *
     */
    public function showAction($id) {
        //==>Session           
//        $session = $this->getRequest()->getSession();
        $user = $this->container->get('security.context')->getToken()->getUser();
        var_dump(gettype($user)=="string");
        if(gettype($user)=="string"){
           return $this->redirect("/login");
        }
//         $l= $user->getEmail();
        $owner = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('kaamhBundle:Experience')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Experience entity.');
        }
        //====================================>khokha 
        //
        //To Calc Rate Average
        $reported = $em->getRepository('kaamhBundle:Report')->findBy(array("user" => $owner, "experience" => $id));
        if ($reported) {
            $report = true;
        } else {
            $report = false;
        }
        //---------------------trendy tags-----------------

        $tagstr = "";
        $helper = new UserHR();
        $result2 = $helper->getTrendyTags($em);

        $expIds = array();
        foreach ($user->getExperiences() as $item) {
            $expIds[] = $item->getId();
        }
        $expstr = implode(',', $expIds);
        if ($expIds !== "") {
            $result3 = $helper->getNotifications($em, $user->getLogoutDate(), $expstr);
        } else {
            $result3 = new \kaamhBundle\Entity\Comment();
        }

        //get latest exp according to user interests 



        $avarage = $this->CalcRate($id);
        //============================================

        if (!$entity->getIsDeleted() || !$entity->getIsHidden()) {

            return $this->render('kaamhBundle:Experience:show.html.twig', array(
                        'entity' => $entity,
                        'owner' => $user,
                        'rates' => $avarage,
                        'reported' => $report,
                        "trendyTag" => $result2,
                        "popularExp" => $result3,
            ));
        } else {
            throw $this->createNotFoundException('Experience Is Deleted');
        }
    }

    /**
     * Displays a form to edit an existing Experience entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('kaamhBundle:Experience')->find($id);
        $regUser = $this->container->get('security.context')->getToken()->getUser();
        if(gettype($regUser)=="string"){
           return $this->redirect("/login");
        }
        $regid = $regUser->getId();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Experience entity.');
        }
        $owner = $entity->getUser()->getId();
        $editForm = $this->createEditForm($entity);


        if ($regid == $owner) {
            return $this->render('kaamhBundle:Experience:edit.html.twig', array(
                        'entity' => $entity,
                        'form' => $editForm->createView(),
//                        'delete_form' => $deleteForm->createView(),
            ));
            
        }else{
            throw $this->createNotFoundException('Your not autherized to edit this experience');
        }


    }

    /**
     * Creates a form to edit a Experience entity.
     *
     * @param Experience $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Experience $entity) {
        $form = $this->createForm(new ExperienceType(), $entity, array(
            'action' => $this->generateUrl('experience_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));
        //==>khokha
        $form->remove('rates'); //To Remove Rating Field

        return $form;
    }

    /**
     * Edits an existing Experience entity.
     * z
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('kaamhBundle:Experience')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Experience entity.');
        }

//        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $oldPic = $entity->getPicture();
        if ($editForm->isValid()) {

//            $session = $this->getRequest()->getSession();
            $user = $this->container->get('security.context')->getToken()->getUser()->getId();
            if(gettype($user)=="string"){
           return $this->redirect("/login");
        }
            $writer = $em->getRepository('kaamhBundle:User')->find($user);
            $entity->setUser($writer);
            if ($editForm['picture']->getData() != "") {
                $entity->setPicture((new UploadImgHR())->UploadImg($editForm, 'experience', 'picture'));
            } else {
                $entity->setPicture($oldPic);
            }
            $em->flush();

            return $this->redirect('/experience/'.$id.'/show');
        }

        return $this->render('kaamhBundle:Experience:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
//                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Experience entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('kaamhBundle:Experience')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Experience entity.');
            }
            $entity->setIsDeleted(1);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('experience'));
    }

    /**
     * Creates a form to delete a Experience entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('experience_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

//======================================>Khokha
//Function To calculate avarage Rate
    private function CalcRate($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('kaamhBundle:Experience')->find($id);
        $allRate = $entity->getRates();
//         var_dump($allRate[0]->getExperience()->getBody());
//         exit();
        $noOfRates = count($allRate);   //Count no of Rates for each experience 

        $sum = 0;
        for ($i = 0; $i < $noOfRates; $i++) {
            $sum = $sum + $allRate[$i]->getRate();
        }
        if ($noOfRates == 0) {
            return $sum;
        } else {
            $avarege = $sum / $noOfRates;
            return $avarege;
        }
    }

    //=================================================
//    private function myexpAction() {
//
//        $session = $this->getRequest()->getSession();
//        $user = $session->get('login');
////         $l= $user->getEmail();
//        $owner = $user->getId();
//        $em = $this->getDoctrine()->getManager();
//        $entities = $em->getRepository('kaamhBundle:Experience')->findBy(array("user_id" => $owner, "isDeleted" => '1'));
//        return $this->render('kaamhBundle:Experience:index.html.twig', array(
//                    'entities' => $entities,
//        ));
//    }
//    private function myexpAction() {
//        $session = new Session();
//        $user = $session->get('login');
////         $l= $user->getEmail();
//        $owner = $user->getId();
//        $em = $this->getDoctrine()->getManager();
//
//        $entities = $em->getRepository('kaamhBundle:Experience')->findfindBy(array("user_id" => $owner));
//
//        return $this->render('kaamhBundle:Experience:index.html.twig', array(
//                    'entities' => $entities,
//        ));
//    }


    public function rateAction(Request $request) {
        $x = $request->get('therate');
        $id = $request->get('expid');
//        $session = $this->getRequest()->getSession();
        $user = $this->container->get('security.context')->getToken()->getUser();
//         
        $owner = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('kaamhBundle:Experience')->find($id);
        $rate = new Rate();
//        $x=8;
        $writer = $em->getRepository('kaamhBundle:User')->find($owner);

        $experience = $em->getRepository('kaamhBundle:Experience')->find($entity->getID());
        $rate->setExperience($experience);
        $rate->setUser($writer);
        $rate->setRate($x);
        $em->persist($rate);
        $em->flush();
        return new Response("Done");
    }

    public function reportAction(Request $request) {


        $data = $request->get('complain');
        $exp = $request->get('expid');
//        $session = $this->getRequest()->getSession();
        $userFrromlogIn = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $report = new Report();
        $report->setComplain($data);
        $user = $em->getRepository('kaamhBundle:User')->find($userFrromlogIn->getId());
        $report->setUser($user);
        $experience = $em->getRepository('kaamhBundle:Experience')->find($exp);
        $report->setExperience($experience);
        $em->persist($report);
        $em->flush();
        return new Response(var_dump($user));
//        
    }

    ///////////////////////Test//
    public function updaterepoAction(Request $request) {

//        $session = $this->getRequest()->getSession();
        $userFrromlogIn = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $exp = $request->get('expid');

        $user = $em->getRepository('kaamhBundle:User')->find($userFrromlogIn->getId());

        $experience = $em->getRepository('kaamhBundle:Experience')->find($exp);

//        $request = $this->getRequest();
//        $id = $request->get('expid');
        $myrate = $em->getRepository('kaamhBundle:Rate')->findBy(array('experience' => $experience, 'user' => $user));
        $em->remove($myrate);
        $em->flush();
        return new Response("done");

//        $data = $request->get('complain');
//        $exp = $request->get('expid');
//        $session = $this->getRequest()->getSession();
//        $userFrromlogIn = $session->get('login');
//
//        $em = $this->getDoctrine()->getManager();
//        $report = new Report();
//        $report->setComplain("NONONONO");
//
//        $user = $em->getRepository('kaamhBundle:User')->find($userFrromlogIn->getId());
//        $report->setUser($user);
//        $experience = $em->getRepository('kaamhBundle:Experience')->find($exp);
////        echo($experience->getUser());
//        $report->setExperience($experience);
//        $em->persist($report);
//        $em->flush();
//        return new Response(var_dump($user));
//        
    }

    ////////////////////////
}
