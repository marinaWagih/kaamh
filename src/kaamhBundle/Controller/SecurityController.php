<?php

namespace kaamhBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use kaamhBundle\Entity;
use kaamhBundle\Helper\EncryptHR;
use kaamhBundle\Form\UserType;
use kaamhBundle\Helper\FaceBookHR;
use kaamhBundle\Helper\UserHR;
use kaamhBundle\Entity\User;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;

class SecurityController extends Controller {

    public function loginAction(Request $request) {

        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('index'));
        } else {

            $authenticationUtils = $this->get('security.authentication_utils');
            // get the login error if there is one
            $error = $authenticationUtils->getLastAuthenticationError();
            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();
            $entity = new User();
            $form = $this->createCreateForm($entity);

            $fbObj2 = new FaceBookHR();
            $LogInURL = $fbObj2->getFacebookUser();
            $regURL = $LogInURL;

            return $this->render('kaamhBundle:Security:login.html.twig', array(
                        'last_username' => $lastUsername,
                        'error' => $error,
                        'entity' => $entity,
                        'form' => $form->createView(),
                        'fblogInURL' => $LogInURL,
                        'fbregURL' => $regURL,
            ));
        }
    }

    public function loginCheckAction() {
        
    }

    private function createCreateForm(User $entity) {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form->remove('isHidden');
        $form->remove('isActive');

//        $form->add('submit', 'submit', array('label' => 'Submit'));


        $form->add('password', 'repeated', array('type' => 'password',
            'invalid_message' => 'The password fields must match.',
            'options' => array('attr' => array('class' => 'password-field')),
            'required' => true,
            'first_options' => array('label' => 'Password'),
            'second_options' => array('label' => 'Repeat Password'),
        ));


        return $form;
    }

    public function logoutAction() {
        //do whatever you want here 
        //clear the token, cancel session and redirect
        $regUser = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('kaamhBundle:User')->find($regUser->getId());
        $user->setLogoutDate();
        $em->flush();
        $this->get('security.context')->setToken(null);
        $this->get('request')->getSession()->invalidate();
//        $cookieName = $this->container->getParameter('framework.session.name');
        $response = $this->redirect($this->generateUrl('login'));


        // Clearing the cookies.
//        $cookieNames = [
//            $this->container->getParameter('neverforget'),
//        ];
//        foreach ($cookieNames as $cookieName) {
            $response->headers->clearCookie('neverforget');
       // }

        return $response;
    }

}
