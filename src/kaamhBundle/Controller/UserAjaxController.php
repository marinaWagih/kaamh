<?php

namespace kaamhBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use kaamhBundle\Entity\User;
use kaamhBundle\Entity\Interest;
use kaamhBundle\Entity\Tag;
use kaamhBundle\Entity\Comment;
use kaamhBundle\Helper\UserHR;

class UserAjaxController extends Controller {

    public function addinterestesAction() {
//        $session = $this->getRequest()->getSession();
        $entity = $this->container->get('security.context')->getToken()->getUser();
        if (gettype($entity) == "string") {
            return $this->redirect("/login");
        }
        $id = $entity->getId();
        $request = $this->getRequest();
        $interestes = $request->get('IntrestesIds');
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('kaamhBundle:User')->find($id);

        for ($i = 0; $i < count($interestes); $i++) {
            $interest = new Interest();
            $interest->setUser($user);
            $tag = $em->getRepository('kaamhBundle:Tag')->find($interestes[$i]);
            $interest->setTag($tag);
            $em->persist($interest);
            $em->flush();
        }
       $this->getRequest()->getSession()->set('justReg',"false");

        return new Response("good");
    }

    public function deleteinterestesAction() {
//        $session = $this->getRequest()->getSession();
        $entity = $this->container->get('security.context')->getToken()->getUser();
        if (gettype($entity) == "string") {
            return $this->redirect("/login");
        }
        $id = $entity->getId();
        $request = $this->getRequest();
        $tag_id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $interest = $em->getRepository('kaamhBundle:Interest')->findOneBy(array('user' => $id, 'tag' => $tag_id));
        $em->remove($interest);
        $em->flush();
        return new Response("good");
    }

    public function addcommentAction() {
        $em = $this->getDoctrine()->getManager();
//        $session = $this->getRequest()->getSession();
        $entity = $this->container->get('security.context')->getToken()->getUser();
        if (gettype($entity) == "string") {
            return $this->redirect("/login");
        }
        $id = $entity->getId();
        $request = $this->getRequest();
        $comment = $request->get('comment');
        $expid = $request->get('expid');
        $user = $em->getRepository('kaamhBundle:User')->find($id);
        $experience = $em->getRepository('kaamhBundle:Experience')->find($expid);
        $mycomment = new Comment();
        $mycomment->setBody($comment);
        $mycomment->setExperience($experience);
        $mycomment->setUser($user);

        $em->persist($mycomment);
        $em->flush();


        return new Response($mycomment->getId());
    }

    public function deletecommentAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $id = $request->get('id');
        $Comment = $em->getRepository('kaamhBundle:Comment')->find($id);
        $em->remove($Comment);
        $em->flush();
        return new Response("done");
    }

    public function editcommentAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $id = $request->get('id');
        $body = $request->get('body');
        $Comment = $em->getRepository('kaamhBundle:Comment')->find($id);
        $Comment->setBody($body);
        $em->persist($Comment);
        $em->flush();
        return new Response("done");
    }

    public function getTagsAction(Request $req) {
        $em = $this->getDoctrine()->getManager();
        $id = $req->get('id') == '0' ? Null : $req->get('id');
        $entities = $em->getRepository('kaamhBundle:Tag')->findBy(array("parent" => $id));
        $response = array();
        foreach ($entities as $item) {
            $response[] = array($item->getId(), $item->getName(), null, null);
        }

        header('Content-type: application/json');
        return new Response(json_encode($response));
    }

    public function deleteExperienceAction() {
//        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $id = $request->get('id');
        $experience = $em->getRepository('kaamhBundle:Experience')->find($id);
        $experience->setIsDeleted('1');
        $em->persist($experience);
        $em->flush();
        return new Response("good");
    }

    public function hideExperienceAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $id = $request->get('id');
        $experience = $em->getRepository('kaamhBundle:Experience')->find($id);
        $hide = $experience->getIsHidden();
        if ($hide=='1') {
            $experience->setIsHidden('0');
            $x=0;
        } else {
            $experience->setIsHidden('1');
            $x=1;
        }
        $em->persist($experience);
        $em->flush();
        return new Response($x);
    }

    public function changePasswordAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $id = $request->get('id');
        $user = $em->getRepository('kaamhBundle:User')->find($id);
        $oldpass = $request->get('oldpass');
        $newpass = $request->get('newpass');
        $encoderFactory = $this->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($user);
        $password = $encoder->encodePassword($oldpass, $user->getSalt());
        if ($password == $user->getPassword()) {
            $mypassword = $encoder->encodePassword($newpass, $user->getSalt());
            $user->setPassword($mypassword);
            $em->persist($user);
            $em->flush();
            $res = "Done";
        } else {
            $res = "Fail";
        }
        return new Response($res);
    }

    public function getNotificationAction() {

        $regUser = $this->container->get('security.context')->getToken()->getUser();
        if (gettype($regUser) == "string") {
            return $this->redirect("/login");
        }
        $em = $this->getDoctrine()->getManager();
        $logOutDate = $regUser->getLogoutDate();
        $helper = new UserHR();
        $expIds = array();
        foreach ($regUser->getExperiences() as $item) {
            $expIds[] = $item->getId();
        }
        $expstr = implode(',', $expIds);
        $result = $helper->getNotifications($em, $logOutDate, $expstr);
        return new Response(json_encode($result));
    }

}
