<?php

namespace kaamhBundle\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use kaamhBundle\Entity\Tag;
//use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use kaamhBundle\Entity\User;
use kaamhBundle\Entity\Experience;
use Symfony\Component\HttpFoundation\Session\Session;
use kaamhBundle\Helper\UserHR;
use kaamhBundle\Form\ExperienceType;


class HomeController extends Controller {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        //addexp
        $experience = new Experience();
        $form = $this->createForm(new ExperienceType(), $experience, array('method' => 'POST'));
        $request = $this->get('request');
        $form->handleRequest($request);
        $repository = $this->getDoctrine()->getRepository('kaamhBundle:Tag');
        $tags = $repository->findAll();
        //
//        $user = $this->container->get('security.context')->getToken()->getUser();
        $regUser = $this->container->get('security.context')->getToken()->getUser();
        $JustReg=$this->getRequest()->getSession()->get('justReg');
        if(gettype($regUser)=="string"){
           return $this->redirect("/login");
        }
        if($JustReg==='true')
        {
            $JustReg="true";
        }
        else
        {
            $JustReg="false";
        }
        
        $user = $em->getRepository('kaamhBundle:User')->find($regUser->getId());
        $interests=$user->getInterests();
        $tagsIds=array();
        $tagstr="";
        //get trendy tags
        $helper=new UserHR();
        $result2=$helper->getTrendyTags($em);
        if(count($interests) > 0)
        {
           
            foreach ($interests as $item )
            {
                $tagsIds[]=$item->getTag()->getId();
            }
            $tagstr=implode(',', $tagsIds);
           // echo $tagstr;
        }
        else
        {
            
            foreach ($result2 as $item )
            {
                $tagsIds[]=$item[0]->getId();
            }
            $tagstr=implode(',', $tagsIds);
          //  echo $tagstr;
        }
//        \Doctrine\Common\Util\Debug::dump($tagsIds);
//        exit();
//        
        $expIds = array();
        foreach ($regUser->getExperiences() as $item) {
            $expIds[] = $item->getId();
        }
        $expstr = implode(',', $expIds);
        if($expIds!=="")
        {
            $result3 = $helper->getNotifications($em,$user->getLogoutDate(),$expstr);
        }
        else
        {
            $result3=new \kaamhBundle\Entity\Comment();
        }
        
        //get latest exp according to user interests 
        
        $result = $helper->getExpAccordingToTags($em, $tagstr);
        
//        
//     \Doctrine\Common\Util\Debug::dump($result);
//     exit();
        $entities = (new UserHR)->getMainTages($em);
        return $this->render('kaamhBundle:Home:index.html.twig', array(
                    "just_Regester" => $JustReg,
                    "Tages" => $entities,
                    "user" => $user,
                    "result"=>$result,
                    "trendyTag"=>$result2,
                    "popularExp"=>$result3,
                    'entity' => $experience,
                    'form' => $form->createView(),
                    'tags' => $tags
        ));
    }

    public function searchAction() {



        return $this->render('kaamhBundle:Home:search.html.twig', array(
                        // ...
//          
        ));
    }

    public function getAllTagsAction(Request $request) {
       
         $regUser = $this->container->get('security.context')->getToken()->getUser();
         if (gettype($regUser) == "string") {
            return $this->redirect("/login");
        }
        $tag=$request->get('search');
        $query = $this->container
                ->get('doctrine')
                ->getEntityManager()
                ->createQuery(
                        'SELECT e FROM kaamhBundle:Tag e WHERE e.name LIKE :search'
                )
                ->setParameter('search', $tag.'%');
        $result = $query->getResult();
//        var_dump($result[0]);
//        return new Response(json_encode($result));
        return $this->redirect('/tag/'.$result[0]->getId().'/show');
    }

}
