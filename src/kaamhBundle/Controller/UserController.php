<?php

namespace kaamhBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use kaamhBundle\Entity\User;
use kaamhBundle\Entity\Tag;
use kaamhBundle\Entity\Interest;
use kaamhBundle\Form\UserType;
use kaamhBundle\Helper\FaceBookHR;
use kaamhBundle\Helper\UserHR;
use kaamhBundle\Helper\UploadImgHR;
use Symfony\Component\HttpFoundation\Session\Session;
use kaamhBundle\Helper\EncryptHR;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * User controller.
 *
 */
class UserController extends Controller {

    /**
     * Lists all User entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('kaamhBundle:User')->findAll();

        return $this->render('kaamhBundle:User:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new User entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        if ($form->isValid()) {
            $imgPath = (new UploadImgHR())->UploadImg($form, 'profilePicture', 'profilePicture');
            $entity->setProfilePicture($imgPath);
            $em = $this->getDoctrine()->getManager();
//            $encPassword=(new EncryptHR())->encrypt($entity->getPassword(),$entity->getSalt());
//            $entity->setPassword($encPassword);

            $encoderFactory = $this->get('security.encoder_factory');
            $encoder = $encoderFactory->getEncoder($entity);
            $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
            $entity->setPassword($password);


            $em->persist($entity);
            $em->flush();
            $token = new UsernamePasswordToken($entity, $entity->getPassword(), "default", array('ROLE_USER'));
            $securityContext = $this->container->get('security.context'); // do it your way
            $securityContext->setToken($token);
            $event = new InteractiveLoginEvent($this->getRequest(), $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
            $this->getRequest()->getSession()->set('_security_secured_area', serialize($token));
//            $email = $form->get('email')->getData();
//            $message = \Swift_Message::newInstance()
//            ->setSubject('Hello Email')
//            ->setFrom('kaamhteam@gmail.com')
//            ->setTo("$email")
//            ->setBody('o.O da atb3t')
//             ;
//             
//            $this->get('mailer')->send($message);

            $this->getRequest()->getSession()->set('justReg', 'true');
            return $this->redirect('/home');
        }
        $fbObj = new FaceBookHR();

        $session = $fbObj->getFacebookUser('reg');
        return $this->render('kaamhBundle:User:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'fbURL' => $session,
        ));
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity) {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form->remove('isHidden');
        $form->remove('isActive');

        $form->add('submit', 'submit', array('label' => 'Create'));

        $form->add('password', 'repeated', array('type' => 'password',
            'invalid_message' => 'The password fields must match.',
            'options' => array('attr' => array('class' => 'password-field')),
            'required' => true,
            'first_options' => array('label' => 'Password'),
            'second_options' => array('label' => 'Repeat Password'),
        ));


        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     */
    public function newAction() {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $fbObj = new FaceBookHR();
        $session = $fbObj->getFacebookUser('reg');
        $error = "";
        if (isset($_GET['error'])) {
            $error = $_GET['error'];
        }

        return $this->render('kaamhBundle:User:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'fbURL' => $session,
                    'error' => $error,
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id) {
        ///
//        $session = new Session();
        $user = $this->container->get('security.context')->getToken()->getUser();
       if (gettype($user) == "string") {
            return $this->redirect("/login");
        }
        $owner = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $experiences = $em->getRepository('kaamhBundle:Experience')->findBy(array('user' => $id, 'isDeleted' => '0'));
        $experiences = array_reverse($experiences);
        $entity = $em->getRepository('kaamhBundle:User')->find($id);
//        $deleteForm = $this->createDeleteForm($id);
        if ($entity) {
            $editForm = $this->createEditForm($entity);
            $entities = (new UserHR)->getMainTages($em);
        } else {
            throw $this->createNotFoundException('This user not exist');
        }



        if (($id == $owner || !$entity->getIsHidden()) && $entity->getIsActive()) {
//            $deleteForm = $this->createDeleteForm($id);
            // $interests = (new UserHR())->getUserTags($entity, $em);
            $exist = ( $owner);
            return $this->render('kaamhBundle:User:show.html.twig', array(
                        'entity' => $entity,
                        "Tages" => $entities,
                        'owner' => $exist,
                        'form' => $editForm->createView(),
                        'experiences' => $experiences,
            ));
        } else {
            throw $this->createNotFoundException('Profile is hidden or deactive');
        }
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction() {
        $em = $this->getDoctrine()->getManager();
//        $session = $this->getRequest()->getSession();
        $regUser = $this->container->get('security.context')->getToken()->getUser();
         if (gettype($regUser) == "string") {
            return $this->redirect("/login");
        }
        $entity = $em->getRepository('kaamhBundle:User')->find($regUser->getId());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('kaamhBundle:User:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity) {
        $user = new UserType();

        $form = $this->createForm($user, $entity, array(
            'action' => $this->generateUrl('user_update'),
            'method' => 'PUT',
        ));

//khokha===>Remove Password and re-password in Edit Form 
        $form->remove('password');
        $form->remove('isActive');
//      
//        
//khokha===>Email un ediatable
        $form->add('email', 'email', array('read_only' => 'true'));
//        $form->add('email', 'email');
        //$x= getAbsolutePath( $entity->getProfilePicture());
        //$form->add('profilePicture','file',array('data_class' =>$entity->getProfilePicture()));
//        $form->add('submit', 'submit', array('label' => 'Submit'));
//        var_dump($entity->getProfilePicture());

        return $form;
    }

    /**
     * Edits an existing User entity.
     *
     */
    public function updateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
//        $session = $this->getRequest()->getSession();
        $regUser = $this->container->get('security.context')->getToken()->getUser();
         if (gettype($regUser) == "string") {
            return $this->redirect("/login");
        }
        $id = $regUser->getId();
        $entity = $em->getRepository('kaamhBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $oldPic = $entity->getProfilePicture();
//        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($editForm['profilePicture']->getData() != "") {
                $imgPath = $imgPath = (new UploadImgHR())->UploadImg($editForm, 'profilePicture', 'profilePicture');
                $entity->setProfilePicture($imgPath);
            } else {
                $entity->setProfilePicture($oldPic);
            }
            $em->flush();

            return $this->redirect($this->generateUrl('user_show', array('id' => $id)));
        }

        return $this->render('kaamhBundle:User:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a User entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('kaamhBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
//    private function createDeleteForm($id) {
//        return $this->createFormBuilder()
//                        ->setAction($this->generateUrl('user_delete', array('id' => $id)))
//                        ->setMethod('DELETE')
//                        ->add('submit', 'submit', array('label' => 'Delete'))
//                        ->getForm()
//        ;
//    }
}
