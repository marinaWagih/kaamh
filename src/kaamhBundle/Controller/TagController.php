<?php

namespace kaamhBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use kaamhBundle\Entity\Tag;
use kaamhBundle\Form\TagType;
use \kaamhBundle\Helper\UploadImgHR;
use kaamhBundle\Helper\UserHR;

/**
 * Tag controller.
 *
 */
class TagController extends Controller {

    /**
     * Lists all Tag entities.
     *
     */
    public function indexAction() {
        $regUser = $this->container->get('security.context')->getToken()->getUser();
        if (gettype($regUser) == "string") {
            return $this->redirect("/login");
        }
        $em = $this->getDoctrine()->getManager();
//        $id =$request->get('id') ? $request->get('id') : NULL;
        $entities = $em->getRepository('kaamhBundle:Tag')->findBy(array('parent' => Null));

        return $this->render('kaamhBundle:Tag:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Tag entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Tag();
        $regUser = $this->container->get('security.context')->getToken()->getUser();
        if (gettype($regUser) == "string") {
            return $this->redirect("/login");
        }
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
//            $session = $this->getRequest()->getSession();
            $user = $this->container->get('security.context')->getToken()->getUser();
            $writer = $em->getRepository('kaamhBundle:User')->find($user);
            $entity->setUser($writer);
            $entity->setPicture((new UploadImgHR())->UploadImg($form, 'tag', 'picture'));
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tag_show', array('id' => $entity->getId())));
        }

        return $this->render('kaamhBundle:Tag:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Tag entity.
     *
     * @param Tag $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Tag $entity) {
        $form = $this->createForm(new TagType(), $entity, array(
            'action' => $this->generateUrl('tag_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Tag entity.
     *
     */
    public function newAction() {
        $entity = new Tag();
        $form = $this->createCreateForm($entity);

        return $this->render('kaamhBundle:Tag:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Tag entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();
        $regUser = $this->container->get('security.context')->getToken()->getUser();
        if (gettype($regUser) == "string") {
            return $this->redirect("/login");
        }
        $entity = $em->getRepository('kaamhBundle:Tag')->find($id);
        $helper = new UserHR();
        $trendyTags = $helper->getTrendyTags($em);
        $expIds = array();
        foreach ($regUser->getExperiences() as $item) {
            $expIds[] = $item->getId();
        }
        $expstr = implode(',', $expIds);
        if ($expIds !== "") {
            $notification = $helper->getNotifications($em, $regUser->getLogoutDate(), $expstr);
        } else {
            $notification = new \kaamhBundle\Entity\Comment();
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tag entity.');
        }

        //  $deleteForm = $this->createDeleteForm($id);

        return $this->render('kaamhBundle:Tag:show.html.twig', array(
                    'entity' => $entity,
                    'trendyTag' => $trendyTags,
                    'notification' => $notification
        ));
    }

    /**
     * Displays a form to edit an existing Tag entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();
        $regUser = $this->container->get('security.context')->getToken()->getUser();
         if (gettype($regUser) == "string") {
            return $this->redirect("/login");
        }
        $entity = $em->getRepository('kaamhBundle:Tag')->find($id);
             
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tag entity.');
        }
        if($entity->getUser()->getId()!==$regUser->getId())
        {
             throw $this->createNotFoundException('you cant edit this you are NOT the owner .');
        }
        $editForm = $this->createEditForm($entity);
        // $deleteForm = $this->createDeleteForm($id);

        return $this->render('kaamhBundle:Tag:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                        //  'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Tag entity.
     *
     * @param Tag $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Tag $entity) {
        $form = $this->createForm(new TagType(), $entity, array(
            'action' => $this->generateUrl('tag_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

//        $form->remove('name');
        $form->remove('parent');
//        $form->add('name', 'text' ,array(
//                                            'data' => $entity->getName(),
//                                            'disabled'=>true
//                                        ));
        $form->add('submit', 'submit', array('label' => 'Update'));
        return $form;
    }

    /**
     * Edits an existing Tag entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
         $regUser = $this->container->get('security.context')->getToken()->getUser();
         if (gettype($regUser) == "string") {
            return $this->redirect("/login");
        }
        $entity = $em->getRepository('kaamhBundle:Tag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tag entity.');
        }
        if($entity->getUser()->getId()!==$regUser->getId())
        {
             throw $this->createNotFoundException('you cant edit this you are NOT the owner .');
        }
        // $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $oldPic = $entity->getPicture();
        if ($editForm->isValid()) {
//            $session = $this->getRequest()->getSession();
            $user = $this->container->get('security.context')->getToken()->getUser();
            $writer = $em->getRepository('kaamhBundle:User')->find($user);
            $entity->setUser($writer);
            if ($editForm['picture']->getData() != "") {
                $entity->setPicture((new UploadImgHR())->UploadImg($editForm, 'tag', 'picture'));
            } else {
                $entity->setPicture($oldPic);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('tag_show', array('id' => $id)));
        }

        return $this->render('kaamhBundle:Tag:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                        //         'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Tag entity.
     *
     */
//    public function deleteAction(Request $request, $id) {
//        $form = $this->createDeleteForm($id);
//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $entity = $em->getRepository('kaamhBundle:Tag')->find($id);
//
//            if (!$entity) {
//                throw $this->createNotFoundException('Unable to find Tag entity.');
//            }
//
//            $em->remove($entity);
//            $em->flush();
//        }
//
//        return $this->redirect($this->generateUrl('tag'));
//    }

    /**
     * Creates a form to delete a Tag entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
//    private function createDeleteForm($id) {
//        return $this->createFormBuilder()
//                        ->setAction($this->generateUrl('tag_delete', array('id' => $id)))
//                        ->setMethod('DELETE')
//                        ->add('submit', 'submit', array('label' => 'Delete'))
//                        ->getForm()
//        ;
//    }
}
