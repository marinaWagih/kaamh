<?php


/**
 * Description of UserHR
 *
 * @author amay
 */
namespace kaamhBundle\Helper;
use kaamhBundle\Entity\User;
use kaamhBundle\Entity\Tag;
class UserHR 
{
    
    public function getMainTages($em)
    {
      $entities = $em->getRepository('kaamhBundle:Tag')->findBy(array('parent'=>null));
      return  $entities;
    }
    public function getNotifications($em,$logOutDate,$expstr)
    {
        
        $query = $em->createQueryBuilder()
                ->select('c')
                ->from('kaamhBundle:Comment', ' c')
                ->where("c.experience IN (:exp)")   
                ->andWhere("c.createDate >  :date")
                ->orderBy('c.createDate', 'desc')
                ->setParameter('exp',$expstr)
                ->setParameter('date', $logOutDate);
        $result = $query->getQuery()->getResult();
        return $result;
    }
    public function getTrendyTags($em)
    {
        $query2 = $em->createQueryBuilder()
                ->select('t , count( exp.id ) AS expCount')
                ->from('kaamhBundle:Tag',' t')
                ->innerJoin('kaamhBundle:TagExperience','te','WITH','t.id = te.tag')
                ->innerJoin('kaamhBundle:Experience','exp','WITH','exp.id = te.experience')
                ->groupBy('t.id')
                ->orderBy('expCount','desc')
                ->setMaxResults(3);
        $result2 = $query2->getQuery()->getResult();
        return $result2;
    }
    public function getPopularExperience($em)
    {
         $query3 = $em->createQueryBuilder()
                ->select('exp , avg( r.rate ) AS avgRate')
                ->from('kaamhBundle:Experience',' exp')
                ->innerJoin('kaamhBundle:Rate','r','WITH','exp.id = r.experience')
                ->groupBy('r.experience')
                ->orderBy('exp.createDate ','desc')
                ->orderBy('avgRate','desc')
                ->setMaxResults(3);
        $result3 = $query3->getQuery()->getResult();
        return $result3;
    }
    public function getExpAccordingToTags($em,$tagstr)
    {
          $query = $em->createQueryBuilder()
                ->select('exp , avg( r.rate ) AS avgRate')
                ->from('kaamhBundle:Experience',' exp')
                ->innerJoin('kaamhBundle:TagExperience','te','WITH','exp.id = te.experience')
                ->innerJoin('kaamhBundle:Rate','r','WITH','exp.id = r.experience')
                ->where('te.tag IN ( :tags )')
                ->andWhere("exp.isHidden = '0'")
                ->andWhere("exp.isDeleted = '0'")
                ->groupBy('r.experience')
                ->orderBy('exp.createDate ','desc')
                ->orderBy('avgRate','desc')
                ->setParameter('tags',$tagstr) 
                ->setMaxResults(10);
        $result = $query->getQuery()->getResult();
        
//     \Doctrine\Common\Util\Debug::dump($result);
//     exit();
        return $result;
    }
//    public function getUserPoints($em,) 
//    {
//          $
//    }

}
