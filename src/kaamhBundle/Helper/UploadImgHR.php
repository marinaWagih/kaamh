<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UploadImgHR
 *
 * @author marina
 */
namespace kaamhBundle\Helper;

class UploadImgHR {
    //put your code here
    public function UploadImg($form,$type,$varName) {
        $dir = "images/".$type;
        $defaultdir = "images/defultPictures";
        $file = $form[$varName]->getData();
        if ($file) {
            $extension = $file->guessExtension();

            if (!$extension) {
                // extension cannot be guessed
                $extension = 'jpg';
            }
            $imgName = rand(1, 99999) . '' . rand(1, 99999) . rand(1, 99999) . '' . rand(1, 99999) . '' . rand(1, 99999);
            $file->move($dir, $imgName . '.' . $extension);
            return $dir . '/' . $imgName . '.' . $extension;
        } else {
            return $defaultdir . '/'.$type.'DefaultPic.png';
        }
    }
}
