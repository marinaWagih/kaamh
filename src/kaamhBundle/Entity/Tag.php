<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Experience
 * @author Kaamh
 * DB Table
 */

namespace kaamhBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="tag")
 * 
 */
class Tag {

    /**
     * @ORM\OneToMany(targetEntity="TagExperience", mappedBy="tag")
     */
    protected $experiences;

    /**
     * @ORM\OneToMany(targetEntity="Interest", mappedBy="tag")
     */
    protected $interests;

    /**
     * @ORM\OneToMany(targetEntity="Tag", mappedBy="parent")
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="Tag", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="Tags")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string",length=45,nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(type="text",nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string",length=100,nullable=true)
     */
    protected $picture;

    /**
     * ORM\@Column(type="datetime",nullable=false) 
     */
    protected $createDate;
   /**
     * @ORM\Column(type="string",length=50,nullable=true)
     */
    protected $color;
    public function __construct() {
        $this->createDate = new \DateTime();
        $this->children = new ArrayCollection();
        $this->experiences = new ArrayCollection();
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }

    function getPicture() {
        return $this->picture;
    }

    function getCreateDate() {
        return $this->createDate;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setPicture($picture) {
        $this->picture = $picture;
    }

    function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * Set user
     *
     * @param \kaamhBundle\Entity\User $user
     * @return Tag
     */
    public function setUser(\kaamhBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \kaamhBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Add children
     *
     * @param \kaamhBundle\Entity\Tag $children
     * @return Tag
     */
    public function addChild(\kaamhBundle\Entity\Tag $children) {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \kaamhBundle\Entity\Tag $children
     */
    public function removeChild(\kaamhBundle\Entity\Tag $children) {
        $this->children->removeElement($children);
    }

    public function removeAllChild() {
        $this->children = NULL;
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \kaamhBundle\Entity\Tag $parent
     * @return Tag
     */
    public function setParent(\kaamhBundle\Entity\Tag $parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \kaamhBundle\Entity\Tag 
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * Add interests
     *
     * @param \kaamhBundle\Entity\Interest $interests
     * @return Tag
     */
    public function addInterest(\kaamhBundle\Entity\Interest $interests) {
        $this->interests[] = $interests;

        return $this;
    }

    /**
     * Remove interests
     *
     * @param \kaamhBundle\Entity\Interest $interests
     */
    public function removeInterest(\kaamhBundle\Entity\Interest $interests) {
        $this->interests->removeElement($interests);
    }

    /**
     * Get interests
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInterests() {
        return $this->interests;
    }


    /**
     * Add experiences
     *
     * @param \kaamhBundle\Entity\TagExperience $experiences
     * @return Tag
     */
    public function addExperience(\kaamhBundle\Entity\TagExperience $experiences)
    {
        $this->experiences[] = $experiences;

        return $this;
    }

    /**
     * Remove experiences
     *
     * @param \kaamhBundle\Entity\TagExperience $experiences
     */
    public function removeExperience(\kaamhBundle\Entity\TagExperience $experiences)
    {
        $this->experiences->removeElement($experiences);
    }

    /**
     * Get experiences
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExperiences()
    {
        return $this->experiences;
    }
}
