<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Experience
 * @author Kaamh
 * DB Table
 */

namespace kaamhBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="tag_experience")
 * 
 */
class TagExperience {

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Experience", inversedBy="tags")
     * @ORM\JoinColumn(name="experience_id", referencedColumnName="id")
     */
    protected $experience;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Tag", inversedBy="experiences")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     */
    protected $tag;
    
    function getTag() {
        return $this->tag;
    }

    function setTag($tag) {
        $this->tag = $tag;
    }

    


    /**
     * Set experience
     *
     * @param \kaamhBundle\Entity\Experience $experience
     * @return TagExperience
     */
    public function setExperience(\kaamhBundle\Entity\Experience $experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return \kaamhBundle\Entity\Experience 
     */
    public function getExperience()
    {
        return $this->experience;
    }
}
