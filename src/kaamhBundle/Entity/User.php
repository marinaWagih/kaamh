<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace kaamhBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Util\SecureRandom;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @UniqueEntity(fields="email", message="Email is already exist in system")
 */
class User implements UserInterface, \Serializable {

    /**
     * @ORM\OneToMany(targetEntity="Report", mappedBy="user")
     */
    protected $reports;

    /**
     * @ORM\OneToMany(targetEntity="Interest", mappedBy="user")
     */
    protected $interests;

    /**
     * @ORM\OneToMany(targetEntity="Rate", mappedBy="user")
     */
    protected $rates;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="user")
     */
    protected $comments;

    /**
     * @ORM\OneToMany(targetEntity="Tag", mappedBy="user")
     */
    protected $tags;

    /**
     * @ORM\OneToMany(targetEntity="Experience", mappedBy="user")
     */
    protected $experiences;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",length=45 , nullable=true)
     */
    protected $userName;

    /**
     * @Assert\NotBlank(
     *  message = "Cann't be blank."
     * )
     * @ORM\Column(type="string",length=45 , nullable=false)
     */
    protected $fristName;

    /**
     * @Assert\NotBlank(
     * message = "Cann't be blank."
     * )
     * @ORM\Column(type="string",length=45 , nullable=false)
     */
    protected $lastName;

    /**
     * @Assert\NotBlank(
     * message = "Cann't be blank."
     * 
     * )
     * @Assert\Regex(
     *   pattern= "#}#",
     *     match=false,
     *     message="Your mail can't contain Special Char"
     * )

     * @ORM\Column(type="string",length=100 , nullable=false , unique=true)
     * 
     */
    protected $email;

    /**
     * @Assert\NotBlank(
     * message = "Cann't be blank."
     * )
     * @Assert\Length(
     *      min = 6,
     *      minMessage = "too short",
     * )
     * @ORM\Column(type="string",length=200 , nullable=true)
     */
    protected $password;

    /**
     * @Assert\NotBlank(
     * message = "Cann't be blank."
     * )
     * @ORM\Column(type="string", columnDefinition="ENUM('male', 'female')" , nullable=false)
     */
    protected $gender;

    /**
     * @ORM\Column(type="string",length=100 , nullable=true)
     */
    protected $profilePicture;

    /**
     * @ORM\Column(type="boolean" )
     */
    protected $isHidden;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isActive;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $faceBookId;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('0', '1')" )
     */
    protected $isAdmin;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createDate;

    /**
     * @ORM\Column(type="string",length=100 )
     */
    protected $salt;

    /**
     * @ORM\Column(type="datetime" , nullable=true)
     */
    protected $logoutDate;

    /**
     * @ORM\Column(type="integer", nullable=true)   
     */
    protected $points;

    public function __construct() {
        $this->isHidden = true;
        $this->isActive = true;
        $this->comments = new ArrayCollection();
        $this->rates = new ArrayCollection();
        $this->experiences = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->interests = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->isAdmin = '0';
        $this->createDate = new \DateTime();
        $this->faceBookToken = "";
//        $generator = ();
        $this->salt = rand(0, 999) . rand(0, 999) . rand(0, 999);
        $this->points=0;
    }

    function getId() {
        return $this->id;
    }

    function getFristName() {
        return $this->fristName;
    }

    function getLastName() {
        return $this->lastName;
    }

    function getEmail() {
        return $this->email;
    }

    function getPassword() {
        return $this->password;
    }

    function getGender() {
        return $this->gender;
    }

    function getProfilePicture() {
        return $this->profilePicture;
    }

    function getIsHidden() {
        return $this->isHidden;
    }

    function getIsActive() {
        return $this->isActive;
    }

    function getFaceBookId() {
        return $this->faceBookId;
    }

    function getIsAdmin() {
        return $this->isAdmin;
    }

    function getCreateDate() {
        return $this->createDate;
    }

    function getLogoutDate() {
        return $this->logoutDate;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFristName($fristName) {
        $this->fristName = $fristName;
    }

    function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    function setEmail($email) {
        $this->email = $email;
        $this->username = $email;
    }

    function setPassword($password) {
//        $this->password = $password;
        //==>Khokha
        // $this->password= hash('sha256', $this->salt);
        $this->password = $password;
        // $this->password = sha1($password);
    }

    function setGender($gender) {
        $this->gender = $gender;
    }

    function setProfilePicture($profilePicture) {
        $this->profilePicture = $profilePicture;
    }

    function setIsHidden($isHidden) {
        $this->isHidden = $isHidden;
    }

    function setIsActive($isActive) {
        $this->isActive = $isActive;
    }

    function setFaceBookId($faceBookId) {
        $this->faceBookId = $faceBookId;
    }

    function setIsAdmin($isAdmin) {
        $this->isAdmin = $isAdmin;
    }

    function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }

    function setLogoutDate() {
        $this->logoutDate = new \DateTime();
    }

    function getPoints() {
        return $this->points;
    }

    function setPoints($points) {
        $this->points = $points;
    }

    function getSalt() {
        return $this->salt;
    }

    function setSalt($salt) {
        $this->salt = $salt;
    }

    /**
     * Constructor
     */
//    public function __construct() {
//        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
//    }

    /**
     * Add comments
     *
     * @param \kaamhBundle\Entity\Comment $comments
     * @return User
     */
    public function addComment(\kaamhBundle\Entity\Comment $comments) {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \kaamhBundle\Entity\Comment $comments
     */
    public function removeComment(\kaamhBundle\Entity\Comment $comments) {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * Add experiences
     *
     * @param \kaamhBundle\Entity\Experience $experiences
     * @return User
     */
    public function addExperience(\kaamhBundle\Entity\Experience $experiences) {
        $this->experiences[] = $experiences;

        return $this;
    }

    /**
     * Remove experiences
     *
     * @param \kaamhBundle\Entity\Experience $experiences
     */
    public function removeExperience(\kaamhBundle\Entity\Experience $experiences) {
        $this->experiences->removeElement($experiences);
    }

    /**
     * Get experiences
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExperiences() {
        return $this->experiences;
    }

    /**
     * Add tags
     *
     * @param \kaamhBundle\Entity\Tag $tags
     * @return User
     */
    public function addTag(\kaamhBundle\Entity\Tag $tags) {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \kaamhBundle\Entity\Tag $tags
     */
    public function removeTag(\kaamhBundle\Entity\Tag $tags) {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags() {
        return $this->tags;
    }

    /**
     * Add rates
     *
     * @param \kaamhBundle\Entity\Rate $rates
     * @return User
     */
    public function addRate(\kaamhBundle\Entity\Rate $rates) {
        $this->rates[] = $rates;

        return $this;
    }

    /**
     * Remove rates
     *
     * @param \kaamhBundle\Entity\Rate $rates
     */
    public function removeRate(\kaamhBundle\Entity\Rate $rates) {
        $this->rates->removeElement($rates);
    }

    /**
     * Get rates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRates() {
        return $this->rates;
    }

    /**
     * Add reports
     *
     * @param \kaamhBundle\Entity\Report $reports
     * @return User
     */
    public function addReport(\kaamhBundle\Entity\Report $reports) {
        $this->reports[] = $reports;

        return $this;
    }

    /**
     * Remove reports
     *
     * @param \kaamhBundle\Entity\Report $reports
     */
    public function removeReport(\kaamhBundle\Entity\Report $reports) {
        $this->reports->removeElement($reports);
    }

    /**
     * Get reports
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReports() {
        return $this->reports;
    }

    /**
     * Add interests
     *
     * @param \kaamhBundle\Entity\Interest $interests
     * @return User
     */
    public function addInterest(\kaamhBundle\Entity\Interest $interests) {
        $this->interests[] = $interests;

        return $this;
    }

    /**
     * Remove interests
     *
     * @param \kaamhBundle\Entity\Interest $interests
     */
    public function removeInterest(\kaamhBundle\Entity\Interest $interests) {
        $this->interests->removeElement($interests);
    }

    /**
     * Get interests
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInterests() {
        return $this->interests;
    }

    public function __toString() {
        return $this->fristName . " " . $this->lastName;
    }

    public function getRoles() {
        return array('ROLE_USER');
    }

    public function eraseCredentials() {
        
    }

    /** @see \Serializable::serialize() */
    public function serialize() {
        return serialize(array(
            $this->id,
            $this->userName,
            $this->password,
                // see section on salt below
                // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized) {
        list (
                $this->id,
                $this->userName,
                $this->password,
                // see section on salt below
                // $this->salt
                ) = unserialize($serialized);
    }

    function getuserName() {
        return $this->userName;
    }

}
