<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Experience
 * @author Kaamh
 * DB Table
 */

namespace kaamhBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="report")
 * 
 */
class Report {

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reports")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Experience", inversedBy="reports")
     * @ORM\JoinColumn(name="experience_id", referencedColumnName="id")
     */
    protected $experience;

    /**
     * @ORM\Column(type="text",nullable=false)
     */
    protected $complain;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createDate;

    public function __construct() {
        $this->createDate = new \DateTime();
    }



    /**
     * Set complain
     *
     * @param string $complain
     * @return Report
     */
    public function setComplain($complain)
    {
        $this->complain = $complain;

        return $this;
    }

    /**
     * Get complain
     *
     * @return string 
     */
    public function getComplain()
    {
        return $this->complain;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     * @return Report
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set user
     *
     * @param \kaamhBundle\Entity\User $user
     * @return Report
     */
    public function setUser(\kaamhBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \kaamhBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set experience
     *
     * @param \kaamhBundle\Entity\Experience $experience
     * @return Report
     */
    public function setExperience(\kaamhBundle\Entity\Experience $experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return \kaamhBundle\Entity\Experience 
     */
    public function getExperience()
    {
        return $this->experience;
    }
}
