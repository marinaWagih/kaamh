<?php

/**Description of Comment
 * @auther Kholoud
 * @Editor Documentation Marina
 * @Editor Relation Amany
 * DB Table
 */

namespace kaamhBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comment")
 */
class Comment {
    
//_____________________________________________________________________________
/**                     FK from user Model   
*           to know which user add this comment without joins 
*/
//_____________________________________________________________________________
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="Comments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
//_____________________________________________________________________________

    
    
/**                      FK from experience Model
*       to know which experience  this comment belongs to without joins 
*/
//_____________________________________________________________________________
    
    /**
     * @ORM\ManyToOne(targetEntity="Experience", inversedBy="Comments")
     * @ORM\JoinColumn(name="experience_id", referencedColumnName="id")
     */
    protected $experience;
//_____________________________________________________________________________
 
    
//_____________________________________________________________________________   
/**                 Comment Model Params
 *          list of all coulmn insde this table in DB
*/
//_____________________________________________________________________________    
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     */
    protected $body;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('0', '1')")
     */
    protected $isDeleted;

    /**
     * @ORM\Column(type="datetime" )
     */
    protected $createDate;
//_____________________________________________________________________________

    
    
//_____________________________________________________________________________   
/**                     Constructor
 * this Constructor to set defults values needed in Comment
*/
//_____________________________________________________________________________
    public function __construct() 
    {
        $this->createDate = new \DateTime();
        $this->isDeleted = "0";
    }
    
//_____________________________________________________________________________
    
    
//_____________________________________________________________________________   
/**                            GETRS
*/
//_____________________________________________________________________________
    function getIsDeleted()
    {
        return $this->isDeleted;
    }

    function getCreateDate()
    {
        return $this->createDate;
    }
    
    function getId()
    {
        return $this->id;
    }

    function getBody()
    {
        return $this->body;
    }
    
    
    /**
     * Get experience
     *
     * @return \kaamhBundle\Entity\Experience 
     */
    
    public function getExperience()
    {
        return $this->experience;
    }

//_____________________________________________________________________________
       
    
    
/**                            SETRS
*/  
//_____________________________________________________________________________
    function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    function setCreateDate($createDate) 
    {

        $this->createDate = $createDate;
    }


    function setId($id)
    {
        $this->id = $id;
    }

    function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Set user
     *
     * @param \kaamhBundle\Entity\User $user
     * @return Comment
     */
    
    public function setUser(\kaamhBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \kaamhBundle\Entity\User 
     */
    
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set experience
     *
     * @param \kaamhBundle\Entity\Experience $experience
     * @return Comment
     */
    
    public function setExperience(\kaamhBundle\Entity\Experience $experience = null)
    {
        $this->experience = $experience;

        return $this;
    }
    
//_____________________________________________________________________________
}
