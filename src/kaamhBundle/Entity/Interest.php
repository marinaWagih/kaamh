<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Experience
 * @author Kaamh
 * DB Table
 */

namespace kaamhBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="interest")
 * 
 */
class Interest {

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="interests")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Tag", inversedBy="interests")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     */
    protected $tag;
    
    function getUser() {
        return $this->user;
    }

    function getTag() {
        return $this->tag;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setTag($tag) {
        $this->tag = $tag;
    }


}
