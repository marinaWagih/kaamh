<?php

/**
 * Description of Experience
 * @author Marina
 * @Editor Documentation Marina
 * @Editor Relation Amany
 * DB Table
 */

namespace kaamhBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="experience")
 * 
 */
class Experience {
//_____________________________________________________________________________  
    /**                     FK from Reports Model
     *              to get this experiance reports without joins
     * */
//_____________________________________________________________________________

    /**
     * @ORM\OneToMany(targetEntity="Report", mappedBy="experience")
     */
    protected $reports;
//_____________________________________________________________________________    
//_____________________________________________________________________________    
    /**                     FK from Rates Model
     *              to get this experiance rates without joins
     * */
//_____________________________________________________________________________   
    /**
     * @ORM\OneToMany(targetEntity="Rate", mappedBy="experience")
     */
    protected $rates;
//_____________________________________________________________________________    
//_____________________________________________________________________________    
    /**                     FK from Tage Model
     *              to get this experiance tages without joins
     * */
//_____________________________________________________________________________ 

    /**
     * @ORM\OneToMany(targetEntity="TagExperience", mappedBy="experience")
     */
    protected $tags;
//_____________________________________________________________________________    
//_____________________________________________________________________________    
    /**                     FK from User Model
     *              to get this experiance Writer without joins
     * */
//_____________________________________________________________________________ 
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="experiences")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;


//_____________________________________________________________________________    
//_____________________________________________________________________________    
    /**                     FK from Comment Model
     *              to get this experiance Comment without joins
     * */
//_____________________________________________________________________________

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="experience")
     */
    protected $comments;


//_____________________________________________________________________________    
//_____________________________________________________________________________    
    /**                      Experience Model Params
     *          list of all coulmn insde this table in DB
     * */
//_____________________________________________________________________________

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text",nullable=false)
     */
    protected $body;

    /**
     * @ORM\Column(type="string",length=100,nullable=true)
     */
    protected $picture;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('0', '1')")
     */
    protected $isHidden;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('0', '1')")
     */
    protected $isDeleted;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createDate;

//_____________________________________________________________________________
//_____________________________________________________________________________   
    /**                     Constructor
     * this Constructor to set defults values needed in Comment
     */
//_____________________________________________________________________________
    public function __construct() {
        $this->comments = new ArrayCollection();
        $this->rates = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->createDate = new \DateTime();
        $this->isDeleted = '0';
        $this->isHidden = '0';
    }

//_____________________________________________________________________________
//_____________________________________________________________________________   
    /**                            GETRS
     */
//_____________________________________________________________________________
    function getId() {
        return $this->id;
    }

    function getBody() {
        return $this->body;
    }

    function getPicture() {
        return $this->picture;
    }

    function getIsHidden() {
        return $this->isHidden;
    }

    function getIsDeleted() {
        return $this->isDeleted;
    }

    function getCreateDate() {
        return $this->createDate;
    }

    public function getComments() {
        return $this->comments;
    }

    /**
     * Get user
     *
     * @return \kaamhBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }



    /**
     * Get rates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRates() {
        return $this->rates;
    }

    /**
     * Get reports
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReports() {
        return $this->reports;
    }

//_____________________________________________________________________________

    /**                            SETRS
     */
//_____________________________________________________________________________



    function setId($id) {
        $this->id = $id;
    }

    function setBody($body) {
        $this->body = $body;
    }

    function setPicture($picture) {
        $this->picture = $picture;
    }

    function setIsHidden($isHidden) {
        $this->isHidden = $isHidden;
    }

    function setIsDeleted($isDeleted) {
        $this->isDeleted = $isDeleted;
    }

    function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }

    /**
     * Set user
     *
     * @param \kaamhBundle\Entity\User $user
     * @return Experience
     */
    public function setUser(\kaamhBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    //_____________________________________________________________________________

    /**
     * Add comments
     *
     * @param \kaamhBundle\Entity\Comment $comments
     * @return Experience
     */
    public function addComment(\kaamhBundle\Entity\Comment $comments) {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \kaamhBundle\Entity\Comment $comments
     */
    public function removeComment(\kaamhBundle\Entity\Comment $comments) {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */



    /**
     * Add rates
     *
     * @param \kaamhBundle\Entity\Rate $rates
     * @return Experience
     */
    public function addRate(\kaamhBundle\Entity\Rate $rates) {
        $this->rates[] = $rates;

        return $this;
    }

    /**
     * Remove rates
     *
     * @param \kaamhBundle\Entity\Rate $rates
     */
    public function removeRate(\kaamhBundle\Entity\Rate $rates) {
        $this->rates->removeElement($rates);
    }

    /**
     * Add reports
     *
     * @param \kaamhBundle\Entity\Report $reports
     * @return Experience
     */
    public function addReport(\kaamhBundle\Entity\Report $reports) {
        $this->reports[] = $reports;

        return $this;
    }

    /**
     * Remove reports
     *
     * @param \kaamhBundle\Entity\Report $reports
     */
    public function removeReport(\kaamhBundle\Entity\Report $reports) {
        $this->reports->removeElement($reports);
    }


    public function __toString() {
        return substr($this->body, 0, 10);
    }



    /**
     * Add tags
     *
     * @param \kaamhBundle\Entity\TagExperience $tags
     * @return Experience
     */
    public function addTag(\kaamhBundle\Entity\TagExperience $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \kaamhBundle\Entity\TagExperience $tags
     */
    public function removeTag(\kaamhBundle\Entity\TagExperience $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

}
