<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Experience
 * @author Kaamh
 * DB Table
 */

namespace kaamhBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="rate")
 * @ORM\HasLifecycleCallbacks
 * 
 */
class Rate {

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="rates")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Experience", inversedBy="rates")
     * @ORM\JoinColumn(name="experience_id", referencedColumnName="id")
     */
    protected $experience;

    /**
     * @ORM\Column(type="integer",nullable=false)
     */
    protected $rate;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createDate;

    public function __construct() {
        $this->createDate = new \DateTime();
    }

    /**
     * Set rate
     *
     * @param integer $rate
     * @return Rate
     */
    public function setRate($rate) {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer 
     */
    public function getRate() {
        return $this->rate;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     * @return Rate
     */
    public function setCreateDate($createDate) {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime 
     */
    public function getCreateDate() {
        return $this->createDate;
    }

    /**
     * Set user
     *
     * @param \kaamhBundle\Entity\User $user
     * @return Rate
     */
    public function setUser(\kaamhBundle\Entity\User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \kaamhBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set experience
     *
     * @param \kaamhBundle\Entity\Experience $experience
     * @return Rate
     */
    public function setExperience(\kaamhBundle\Entity\Experience $experience) {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return \kaamhBundle\Entity\Experience 
     */
    public function getExperience() {
        return $this->experience;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function updatePoints() {

        if ($this->rate >= 0 && $this->rate <= 2) {
            $point = 1;
            
        } elseif ($this->rate >= 3 && $this->rate <= 4) {
            $point = 2;
        } elseif ($this->rate == 5) {
            $point = 3;
        } else {
            $point = 0;
        }
        $oldPoints=$this->experience->getUser()->getPoints();
        $this->experience->getUser()->setPoints($oldPoints+$point);
    }

}
